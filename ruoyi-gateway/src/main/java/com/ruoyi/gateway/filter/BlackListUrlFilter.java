package com.ruoyi.gateway.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import com.ruoyi.common.core.utils.ServletUtils;

/**
 * 黑名单过滤器
 *  继承了 AbstractGatewayFilterFactory 抽象类，并将泛型参数 <C> 设置为我们定义的 BlackListUrlFilter.Config 配置类。
 *  这样，Gateway 在解析配置时，会转换成 Config 对象。
 *  注意：在 AuthGatewayFilterFactory 构造方法中，也需要传递 Config 类给父构造方法，保证能够正确创建 Config 对象。
 * 
 * @author ruoyi
 */
@Component
public class BlackListUrlFilter extends AbstractGatewayFilterFactory<BlackListUrlFilter.Config>
{
    /**
     * 和blacklistUrl匹配的请求路径，将被拦截
     * @param config 1
     * @return: org.springframework.cloud.gateway.filter.GatewayFilter
     * @author: gefeng
     * @date: 2022/6/20 15:41
     */
    @Override
    public GatewayFilter apply(Config config)
    {
        return (exchange, chain) -> {

            String url = exchange.getRequest().getURI().getPath();
            if (config.matchBlacklist(url))
            {
                return ServletUtils.webFluxResponseWriter(exchange.getResponse(), "请求地址不允许访问");
            }

            return chain.filter(exchange);
        };
    }

    public BlackListUrlFilter()
    {
        super(Config.class);
    }

    public static class Config
    {
        private List<String> blacklistUrl; //配置的url黑名单

        private List<Pattern> blacklistUrlPattern = new ArrayList<>(); //配置的url黑名单对应的匹配模式

        public boolean matchBlacklist(String url)
        {
            return !blacklistUrlPattern.isEmpty() && blacklistUrlPattern.stream().anyMatch(p -> p.matcher(url).find());
        }

        public List<String> getBlacklistUrl()
        {
            return blacklistUrl;
        }

        public void setBlacklistUrl(List<String> blacklistUrl)
        {
            this.blacklistUrl = blacklistUrl;
            this.blacklistUrlPattern.clear();
            this.blacklistUrl.forEach(url -> {
                this.blacklistUrlPattern.add(Pattern.compile(url.replaceAll("\\*\\*", "(.*?)"), Pattern.CASE_INSENSITIVE));
            });
        }
    }

}
