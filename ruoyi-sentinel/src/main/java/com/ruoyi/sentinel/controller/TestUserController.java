package com.ruoyi.sentinel.controller;

import com.ruoyi.sentinel.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestUserController
{
    @Autowired
    private IUserService userService;

    @GetMapping("/info/{username}")
    public Object info(@PathVariable("username")String username)
    {
        return userService.selectUserByName(username);
    }
}
