package com.ruoyi.sentinel.service;

public interface IUserService {
    public Object selectUserByName(String username);
}
