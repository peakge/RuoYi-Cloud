package com.ruoyi.test;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Feign 客户端配置
 * @return:
 * @author: gefeng
 * @date: 2022/5/19 19:28
 */
@Configuration
public class FeignConfiguration {

    @Bean
    Logger.Level feignLoggerLevel()
    {
        return Logger.Level.FULL;
    }
}
