package com.ruoyi.test.controller;

import com.ruoyi.test.RemoteUserService;
import com.ruoyi.test.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RemoteUserService remoteUserService;

    /**
     * 获取当前用户信息
     */
    @GetMapping("/user/{username}")
    public Object info(@PathVariable("username") String username)
    {
        return remoteUserService.getUserInfo(username);
    }

    @GetMapping("/user/pojo")
    public Object UserInfo(SysUser user)
    {
        return remoteUserService.selectUser(user);
    }
}
