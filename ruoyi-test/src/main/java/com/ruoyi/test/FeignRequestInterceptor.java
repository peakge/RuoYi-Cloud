package com.ruoyi.test;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * feign拦截器
 * @return:
 * @author: gefeng
 * @date: 2022/5/19 20:04
 */
public class FeignRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        //在feign的http请求头中加入参数token=123456
        requestTemplate.header("token","123456");
    }
}
