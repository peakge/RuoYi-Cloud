package com.ruoyi.test;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 用户服务
 * 
 * @author ruoyi
 */
@FeignClient(contextId = "remoteUserService", value = "ruoyi-system", fallbackFactory = RemoteUserFallbackFactory.class, configuration = FeignConfiguration.class)
public interface RemoteUserService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public Object getUserInfo(@PathVariable("username") String username);

    @GetMapping("/user/pojo")
    public Object selectUser(SysUser user);

//    /**
//     * 注册用户信息
//     *
//     * @param sysUser 用户信息
//     * @param source 请求来源
//     * @return 结果
//     */
//    @PostMapping("/user/register")
//    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
